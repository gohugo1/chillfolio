+++
title = 'Yundon Diary'
date = 2023-01-15T09:00:00-07:00
draft = false
summary = "Yundon Diary is a personal project build with Flutter. It is a fitness app that tracks push-up reps automatically using phone's proximity sensor. "
coverIntro = "A fitness app that automatically tracks your pushups."
cover = "/png/yundon_cover.png"
liveLink = "https://play.google.com/store/apps/details?id=com.wuyuanchen.yundondiary&hl=en_US&gl=US&pli=1"
codeLink = ""
techStack = [ 
    "Flutter",
    "Dart",
    "Hive Database",
    "table_calendar plugin",
    "Android",
    "IOS",
]
+++

 Yundon Diary helps you track pushup repetitions automatically by placing your phone below your chest. It uses proximity sensor to detect light sensitivity as you lower yourself to complete a repetition.

    Feature:
    - Automatically records reps using proximity sensor.
    - Responsive calendar view to log total reps per day.
    - Allow user to input past data or future data.
    - Integrated with Google Admob to display ads at the top banner.
