+++
title = 'Jobflow'
date = 2023-03-15T11:00:00-07:00
draft = false
summary = "Jobflow is a fullstack web application build using React and Django. The idea is to streamlining the job search experience, specifically for software engineering positions. To do this, I am consolidating all the jobs into one place rather than going to various websites for job search."
coverIntro = "A platform that helps software engineers to find jobs."
cover = "/jpeg/jobflow_cover.jpeg"
liveLink = "http://jobflow.wuyuanchen.com/"
codeLink = "https://gitlab.com/all-in-one-job-feed"
techStack = [ 
    "React Typescript",
    "Django Rest Framework",
    "sqlite",
    "jwt token",
    "Docker",
]
+++

<!-- ![Bryce Canyon National Park](bryce-canyon.jpg) -->
While job searching, I always had to visit different sites such as LinkedIn, Indeed, Glassdoor, etc., to search for jobs. The idea is to scrape all these sites and post the relevant jobs into one place. Currently, the app can scrape jobs, display jobs, allow user to bookmark jobs. The frontend is build using React Typescript, backend is build using Django Rest Framework. 