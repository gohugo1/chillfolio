+++
title = 'Utility Tree'
date = 2023-02-15T10:00:00-07:00
draft = false
summary = "This is a static site build using Hugo. I created a Hugo theme, which allows customization through markdown files. The idea is to create Hugo templates so that it could be used by different needs. "
coverIntro = "A Hugo build static site."
cover = "/png/utility_tree_cover.png"
liveLink = "https://utilitytree.netlify.app/"
codeLink = "https://gitlab.com/gohugo1/utility_tree_site"
techStack = [ 
    "HTML",
    "CSS",
    "Javascript",
    "Hugo",
    "Bootstrap 5",
]
+++
Static site generators offer several benefits, including improved performance, security, and simplicity. They pre-build the website's pages, eliminating the need to generate content on-the-fly when a user accesses a page. This results in faster page load times and reduced server load.

This project helped me understand how to create a customizable Hugo theme using Bootstrap 5. 